# Requirement
- java JRE https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
- mozilla firefox https://www.mozilla.org/en-US/firefox/new/
- nodeJS https://nodejs.org/en/

# Installation
- git clone [url]
- npm install
- download geckodriver for your OS https://github.com/mozilla/geckodriver/releases
- cp [downloaded file] ./bin/geckodriver
- cp .env.example .env
- edit your .env base on your profile
- npm run e2e-setup
- npm run letsgo
- saat browser kebuka
- preferences > Privacy & Security
- check always Tracking Protection
- check Block pop-up windows