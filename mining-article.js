const axios = require('axios');
const fs = require('fs');
const path = require('path');
const projectDir = path.resolve(__dirname);

const jsonPath = path.join(projectDir, 'list-article.json');
const jsonNewestPath = path.join(projectDir, 'dump-load', 'list-article-update-' + new Date() + '.json');

var data = [];
var dataFiltered = [];
function makeCall(offset = 0){
	axios.post('https://www.togetherwhatever.id/article-loadmore', {
		category: 'all',
		sorting: 'all',
		offset: offset
	})
	.then(function (response) {
		if(response.data.article_data.length >= 1) {
			let map = response.data.article_data.map(function(e) {
				if(e.article_category == null){
					return 'https://www.togetherwhatever.id/article/' + e.slug
				} else {
					return 'https://www.togetherwhatever.id/article/' + e.article_category.slug + '/' + e.slug
				}
			});
      		var obj = JSON.parse(fs.readFileSync(projectDir + '/list-article.json', 'utf8'));
			for(let i in map) {
				data.push(map[i]);
				if(obj.indexOf(map[i]) == -1) {
					console.log('new Article Boi : ' + map[i]);
					dataFiltered.push(map[i]);	
				}
			}
			console.log('offset : ' + offset);
			offset += 6;
			makeCall(offset);
		} else {
			var json = JSON.stringify(data);
			var jsonFiltered = JSON.stringify(dataFiltered);
			console.log('Article barunya ada : ' + dataFiltered.length);
			console.log('Total Article : ' + data.length);

			fs.writeFile(jsonPath, json, 'utf8');
			fs.writeFile(jsonNewestPath, jsonFiltered, 'utf8');
		}
	}).catch(function(e){
		console.log(e);
	})
}
// call first
makeCall(0);