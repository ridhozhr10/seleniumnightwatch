require('dotenv').config()
const fs = require('fs');
const path = require('path');
const projectDir = path.resolve(__dirname);

module.exports = {
  'TheBots - With Guide' : function (browser) {
    var prefixCat = 'daytripper';
    var category_id = 'all';
    var firstUrl = process.env.FIRSTLINK;
    var articleHitsCount = process.env.HITCOUNT;
    browser
      .url('https://www.togetherwhatever.id/')

      // login part
      .click('a.sign-in-nav')
      .click('.dropdown-menu a[href$=\'login\']')
      .waitForElementVisible('input[name=email]', 500)
      .setValue('input[name=email]', process.env.LOGIN_EMAIL)
      .waitForElementVisible('input[name=password]', 50)
      .setValue('input[name=password]', process.env.LOGIN_PASS)
      .click('input[type=submit]')
      .pause(1000);

      // first article - semua article
      var obj = JSON.parse(fs.readFileSync(projectDir + '/../list-article.json', 'utf8'));

      // kalau mau pakai article baru
      // var obj = JSON.parse(fs.readFileSync(projectDir + '/../dump-load/list-article-update-Tue Oct 30 2018 15:36:35 GMT+0700 (WIB).json', 'utf8'));
      browser.url(obj[0])
      .waitForElementVisible('.like-article', 1000)
      .click('.like-article.default.like-article-ajax')
      .click('div.st-custom-button[data-network=\'facebook\']')
      .pause(1500)
      
      // like segment
      for (let i = 1; i < obj.length; i++) {
        browser
          .url(obj[i])
          .waitForElementVisible('.like-article', 1000)
          .click('.like-article.default.like-article-ajax')
          .click('div.st-custom-button[data-network=\'facebook\']')
          .pause(1500);
      }

      browser
      .end();
  }
};