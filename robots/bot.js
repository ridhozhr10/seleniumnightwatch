require('dotenv').config()

module.exports = {
  'TheBots' : function (browser) {
    var prefixCat = 'daytripper';
    var category_id = 'all';
    var firstUrl = process.env.FIRSTLINK;
    var articleHitsCount = process.env.HITCOUNT;
    browser
      .url('https://www.togetherwhatever.id/')

      // login part
      .click('a.sign-in-nav')
      .click('.dropdown-menu a[href$=\'login\']')
      .waitForElementVisible('input[name=email]', 500)
      .setValue('input[name=email]', process.env.LOGIN_EMAIL)
      .waitForElementVisible('input[name=password]', 50)
      .setValue('input[name=password]', process.env.LOGIN_PASS)
      .click('input[type=submit]')
      .pause(1000)

      // first article
      .url(firstUrl)
      .waitForElementVisible('.like-article', 1000)
      .click('.like-article.default.like-article-ajax')
      .click('div.st-custom-button[data-network=\'facebook\']')
      .pause(500)
      .click('.p-artdet-prev-next a[href^=\'article\']')
      
      // like segment
      for(let i = 0; i < articleHitsCount; i++){
        browser
          .waitForElementVisible('.like-article', 1000)
          .click('.like-article.default.like-article-ajax')
          .click('div.st-custom-button[data-network=\'facebook\']')
          .pause(500)
          .click('.p-artdet-prev-next a[href^=\'article\']')
          .pause(150)
      }
      browser
      .end();
  }
};