
const fs = require('fs');
const path = require('path');
const projectDir = path.resolve(__dirname);
const jsonPath = path.join(projectDir, 'list-trivia.json');
const jsonSortedPath = path.join(projectDir, 'list-trivia-sort.json');

var existing = JSON.parse(fs.readFileSync(jsonPath, 'utf8'));
var json = existing.sort(function (a, b) {
    return parseFloat(a.id_question) - parseFloat(b.id_question);
});
json = JSON.stringify(json);
fs.writeFile(jsonSortedPath, json, 'utf8', () => {
    console.log('writed');
});