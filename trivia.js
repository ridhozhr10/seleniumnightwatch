const axios = require('axios');
const baseUrl = "https://www.togetherwhatever.id/games/trivia";
const baseUrlCheck = "https://www.togetherwhatever.id/games/trivia/checkanswer";

const fs = require('fs');
const path = require('path');
const projectDir = path.resolve(__dirname);
const jsonPath = path.join(projectDir, 'list-trivia.json');

axios.get(baseUrl).then(({ data }) => {
    for (let i = 0; i < data.length; i++) {
        const dataQuestion = data[i];
        const trivia_answer = dataQuestion.trivia_answer;
        for (let e = 0; e < trivia_answer.length; e++) {
            const answer_id = trivia_answer[e].id;
            axios({
                method: 'post',
                url: baseUrlCheck,
                data: {
                    "anid": answer_id
                }
            }).then(({ data }) => {
                if (data.is_right == '1') {
                    var pushedFixedData = {
                        "id_answer": null,
                        "id_question": dataQuestion.id,
                        "score": 4
                    };
                    var existing = JSON.parse(fs.readFileSync(jsonPath, 'utf8'));
                    var lanjutin = true;
                    for (let obj of existing) {
                        if (obj.id_question == dataQuestion.id) {
                            lanjutin = false;
                        }
                    }
                    if (lanjutin) {
                        pushedFixedData.id_answer = answer_id;
                        existing.push(pushedFixedData);
                        var json = JSON.stringify(existing);
                        fs.writeFile(jsonPath, json, 'utf8', () => {
                            console.log('writed');
                        });
                    }
                }
            }).catch((error) => console.error(error));
        }
    }
});